.. title: Call for Papers
.. slug: call-for-papers
.. date: 2019-02-13 00:00:00
.. tags: conference
.. author: . 
.. link: https://www.piratecare.net/



## Centre for Postdigital Cultures <br/> 2nd Annual Conference

# PIRATE CARE

## 19 & 20 June 2019 <br/> Square One, Coventry University


*Submissions for the screening programme also welcome.*  
*(see submission instructions at the end of the call)*

<br/>
<br/>
The [Centre for Postdigital Cultures (CPC)](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/), [Coventry University](https://www.coventry.ac.uk/), UK invites contributions to its second annual conference, which will explore the phenomenon of 'Pirate Care'. The term Pirate Care condenses two processes that are particularly visible at present. On the one hand, basic care provisions that were previously considered cornerstones of social life are now being pushed towards illegality, as a consequence of geopolitical reordering and the marketisation of social services. At the same time new, technologically-enabled care networks are emerging in opposition to this drive toward illegality.

Punitive neoliberalism (Davies, 2016) has been repurposing, rather than dismantling, welfare state provisions such as healthcare, income support, housing and education (Cooper, 2017: 314). This mutation is reintroducing 'poor laws' of a colonial flavour, deepening the lines of discrimination between citizens and non-citizens (Mitropolous, 2012: 27), and reframing the family unit as the sole bearer of responsibility for dependants.

However, against this background of institutionalised 'negligence' (Harney & Moten, 2013: 31), a growing wave of mobilizations around care can be witnessed across a number of diverse examples: the recent Docs Not Cops campaign in the UK, refusing to carry out documents checks on migrant patients; migrant-rescue boats (such as those operated by Sea-Watch) that defy the criminalization of NGOs active in the Mediterranean; and the growing resistance to homelessness via the reappropriation of houses left empty by speculators (like PAH in Spain); the defiance of legislation making  homelessness illegal (such as Hungary's reform of October 2018) or municipal decrees criminalizing helping out in public space (e.g. Food Not Bombs' volunteers arrested in 2017) .

On the other hand, we can see initiatives experimenting with care as collective political practices have to operate in the narrow grey zones left open between different technologies, institutions and laws in an age some fear is heading towards 'total bureaucratization' (Graeber, 2015: 30). For instance, in Greece, where the bureaucratic measures imposed by the Troika decimated public services, a growing number of grassroots clinics set up by the Solidarity Movement have responded by providing medical attention to those without a private insurance. In Italy, groups of parents without recourse to public childcare are organizing their own pirate kindergartens (Soprasotto), reviving a feminist tradition first experimented with in the 1970s. In Spain, the feminist collective GynePunk developed a biolab toolkit for emergency gynaecological care, to allow all those excluded from the reproductive medical services  -  such as trans or queer women, drug users and sex workers  - to perform basic checks on their own bodily fluids. Elsewhere, the collective Women on Waves delivers abortion pills from boats harboured in international waters - and more recently, via drones - to women in countries where this option is illegal.

Thus pirate care, seen in the light of these processes - choosing illegality or existing in the grey areas of the law in order to organize solidarity - takes on a double meaning: Care as Piracy and Piracy as Care (Graziano, 2018).

There is a need to revisit piracy and its philosophical implications - such as sharing, openness, decentralization, free access to knowledge and tools (Hall, 2016) - in the light of transformations in access to social goods brought about by digital networks. It is important to bring into focus the modes of intervention and political struggle that collectivise access to welfare provisions as acts of custodianship (Custodians.online, 2015) and commoning (Caffentzis & Federici, 2014). As international networks of tinkerers and hackers are re-imagining their terrain of intervention, it becomes vital to experiment with a changed conceptual framework that speaks of the importance of the digital realm as a battlefield for the re-appropriation of the means not only of production, but increasingly, of social reproduction (Gutiérrez Aguilar *et al.*, 2016). More broadly, media representations of these dynamics - for example in experimental visual arts and cinema - are of key importance. Bringing the idea of pirate ethics into resonance with contemporary modes of care thus invites different ways of imagining a paradigm change, sometimes occupying tricky positions vis-à-vis the law and the status quo.

The present moment requires a non-oppositional and nuanced approach to the mutual implications of care and technology (Mol et al., 2010: 14), stretching the perimeters of both. And so, while the seminal definition of care distilled by Joan Tronto and Berenice Fisher sees it as 'everything that we do to maintain, continue, and repair "our world" so that we can live in it as well as possible' (Tronto & Fisher, 1990: 40), contemporary feminist materialist scholars such as Maria Puig de La Bellacasa feel the need to modify these parameters to include 'relations \[that\] maintain and repair a world so that humans and non-humans can live in it as well as possible in a complex life-sustaining web' (Puig de La Bellacasa, 2017: 97). It is in this spirit that we propose to examine how can we learn to compose (Stengers, 2015) answers to crises across a range of social domains, and alongside technologies and care practices.

We invite proposals for 20 minute presentations on the theme of Pirate
Care as outlined above. We welcome submissions addressing a wide range
of topics in response to one or more of the following sub-themes:

  * **Criminalisation of Care:** including responses to legal attacks to NGO work in the Mediterranean; state-sanctioned violence against healthcare practitioners (Buissonniere, Woznick, and Rubenstein, 2018); the erosion of reproductive medicine provisions and self-determination rights for women; campaigns to decriminalize sexwork and regularize domestic workers.

  * **Care Struggles:** histories of grassroots and autonomous organizing around care / for access to care. Examples might include histories of workers' mutualism; Black Panthers' free clinics;  ACT UP and AIDS organizing around medical research; feminist struggles for free abortion rights; marginalized constituencies and underground solidarity
networks.

  * **Hacking Care:** care practice in relation to technologies and tools, open softwares and oppositions to the patent regimes. Relevant stories might include: open source medicine; right to repair and medical devices; open pharma; open science; biohacking practices.

 * **Piracy as Care:** focused on practices of civil disobedience that deliberately defy intellectual property and other laws in order to care for practices, ecologies, or constituencies. Examples include shadow libraries' use of internet to support or coordinate around specific social reproductive needs; tinkering and readaptation of technological objects; and digitally-supported systems to support better care of common goods.

We welcome contributions from academics, practitioners, artists, and activist alike. The programme of talks will be accompanied by a film programme addressing the conference theme. Film submissions for inclusion are also welcome.

Proposed contributions for papers should include:

* a presentation title;
* a short abstract (max 350 words);
* a short biographical note ( max 150 words)

Proposed contributions from artists and filmmakers should include:

* a presentation title and brief synopsis (max 250 words);
* a link to the work;
* a short biographical note ( max 150 words)

Please be aware that our facilities will allow for a proper theatrical screening; the digital format is preferred but you can reach out the conference organisers at the email below in case your prospect submission is in other formats.

Please send your submission no later than **April 1, 2019** to [piratecare@gmail.com](mailto:piratecare@gmail.com)

A notification of acceptance will be circulated by mid-April 2019.

Limited travel funding will be made available to conference participants on a needs-based basis. Details on how to apply for this will be made available following paper acceptance.

The conference will be a child-friendly environment.

## About the CPC

The Centre for Postdigital Cultures (CPC) explores how innovations in postdigital cultures can help us to rethink our ways of being and doing in the 21st century. Our research draws on cross-disciplinary ideas associated with open and disruptive media, the posthumanities, and the Anthropocene to promote a more just and sustainable 'post-capitalist' knowledge economy.

See
[https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/)
for more detail.

## References:

Aguilar R.G., Linsalata L. and M.L.N. Trujillo, 2016. 'Producing the common and reproducing life: Keys towards rethinking *the Political.*' In: Dinerstein A. (eds) *Social Sciences for an Other Politics*. Palgrave Macmillan.

Buissonniere,M., S. Woznick, and L. Rubenstein, 2018. 'The Criminalization of healthcare', University of Essex, [https://www1.essex.ac.uk/hrc/documents/54198-criminalization-of-healthcare-web.pdf](https://www1.essex.ac.uk/hrc/documents/54198-criminalization-of-healthcare-web.pdf).

Caffentzis, G. and Federici, S., 2014. Commons against and beyond capitalism. *Community Development Journal*, *49*(suppl\_1), pp.i92-i105.

Cooper, M., 2017. *Family values: Between neoliberalism and the new social conservatism*. MIT Press.

Custodians Online, 2015. 'In solidarity with Library Genesis and Sci-Hub', 30th November, [http://custodians.online/](http://custodians.online/).

Davies, W., 2016. 'The new neoliberalism'. *New Left Review* (101), 121--134

de La Bellacasa, M.P., 2017. *Matters of care: Speculative ethics in more than human worlds* (Vol. 41). University of Minnesota Press.

Fisher, B. and J. C. Tronto, 1990. 'Toward a feminist theory of care', in *Circles of Care: Work and Identity in Women's Lives*, ed. Emily K. Abel and Margaret K. Nelson, Albany: SUNY Press.

Graeber, D., 2015. *The utopia of rules: On technology, stupidity, and the secret joys of bureaucracy*. Melville House.

Graziano, V. 2018. 'Pirate Care - How do we imagine the health care for the future we want?', *Medium*, 5th October [https://medium.com/dsi4eu/pirate-care-how-do-we-imagine-the-health-care-for-the-future-we-want-fa7f71a7a21](https://medium.com/dsi4eu/pirate-care-how-do-we-imagine-the-health-care-for-the-future-we-want-fa7f71a7a21).

Hall, G., 2016. *Pirate philosophy: for a digital posthumanities*. MIT Press.

Harney, S. and Moten, F., 2013. *The undercommons: Fugitive planning and black study*, Minor Compositions.

Mitropoulos, A., 2012. *Contract & contagion: From biopolitics to oikonomia*. Minor Compositions.

Mol, A., Moser, I. and Pols, J. eds., 2015. *Care in practice: On tinkering in clinics, homes and farms* (Vol. 8). transcript Verlag.

Stengers, I. (2015) *In Catastrophic times: Resisting the coming barbarism*. Open Humanities Press.
