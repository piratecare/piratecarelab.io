.. title: About Centre for Postdigital Cultures
.. slug: about-cpc
.. date: 2019-02-14 00:00:00
.. author: . 
.. description:

The Centre for Postdigital Cultures (CPC) explores how innovations in postdigital cultures can help us to rethink our ways of being and doing in the 21st century. Our research draws on cross-disciplinary ideas associated with open and disruptive media, the posthumanities, and the Anthropocene to promote a more just and sustainable 'post-capitalist' knowledge economy.

See
[https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/)
for more detail.

