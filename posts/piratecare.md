#**PIRATE CARE**

##Centre for Postdigital Cultures 2nd Annual Conference
19 & 20 June 2019
Square One, Coventry University



#[GO TO THE CONFERENCE VIDEO DOCUMENTATION](https://www.youtube.com/watch?v=c07WIOwzHQM&list=PLX-N8krB2JMcVktrreeqLJAKTYqNoiSnO)



*NOTE: This site is an archive only. For current news about the *Pirate.Care* project, please visit [pirate.care](https://pirate.care/)




![](/images/kelly_gallegher.png)

*Image credit: Kelly Gallagher*

<br/>
[The Centre for Postdigital Cultures](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/), Coventry University, UK invites you to its second annual conference, which will explore the phenomenon of 'Pirate Care'. The term Pirate Care condenses two processes that are particularly visible at present. On the one hand, basic care provisions that were previously considered cornerstones of social life are now being pushed towards illegality, as a consequence of geopolitical reordering and the marketisation of social services. At the same time new, technologically-enabled care networks are emerging in opposition to this drive toward illegality. The conference will feature projects providing various forms of pirate care ranging from refugee assistance, healthcare, reproductive care, childcare, access to public transport, access to knowledge, a number of reflections from and on such practices, and a film programme.

**_Projects_**: [Cassandra Press](https://www.cassandrapress.org/about) |  [Docs Not Cops](http://www.docsnotcops.co.uk/) + [Medact](https://www.medact.org/) +[#PatientsNotPassports](https://patientsnotpassports.co.uk/) | [The Four Thieves Vinegar Collective](https://fourthievesvinegar.org/) | [Memory of the World](https://www.memoryoftheworld.org/) | [Planka](https://planka.nu/eng/) | [Power Makes Us Sick (PMS)](https://pms.hotglue.me/) | [Sea-Watch](https://sea-watch.org/en/) | [Soprasotto pirate kindergarten](https://soprasottomilano.it/) | [WeMake Milan](http://wemake.cc/) + [Opencare.cc](http://opencare.cc/) | [Conflict, Memory, Displacement project](http://conflictmemorydisplacement.com/).

**_Participants_**: [Agustina Andreoletti](http://www.agustinaandreoletti.com/info/) (Academy of Media Arts Cologne) | [Mijke van der Drift](https://goldsmiths.academia.edu/MijkevanderDrift) (Goldsmiths / Royal Academy of Art, The Hague) | [Taraneh Fazeli](https://www.bemiscenter.org/residency/by_year/2018/taraneh-fazeli.html) (curatorial fellow, Jan van Eyck Academie and Canaries collective) | [Kirsten Forkert](https://www.bcu.ac.uk/media/research/research-staff/kirsten-forkert) (BCU) + [Janna Graham](https://www.gold.ac.uk/visual-cultures/staff/graham-janna/) (Goldsmiths) + Victoria Mponda[Global Sistaz United collective](https://www.globalsistazunited.com/) + (Conflict, Memory, Displacement project) | [Maddalena Fragnito](https://www.maddalenafragnito.com/) (Soprasotto) |  [Valeria Graziano](https://hcommons.org/members/valerix/) (CPC) | [Derly Guzman](https://planka.nu/eng/) | [Toufic Haddad](https://cbrl.ac.uk/news/item/name/cbrl-s-kenyon-institute-welcomes-a-new-deputy-director) (Kenyon Institute) | [Jelka Kretzschmar + Franziska Wallner](https://sea-watch.org/en/) (Sea-Watch) | [Andrea Liu](http://replaceandrea.blogspot.com/) (Goldsmiths) | [Marcell Mars](https://monoskop.org/Marcell_Mars) + [Tomislav Medak](http://tom.medak.click/en/bio/) (CPC) | [Power Makes Us Sick (PMS)](https://pms.hotglue.me/) | [Gilbert B. Rodman](https://www.gilrodman.com/) (University of Minnesota) | [Zoe Romano](https://zoescope.wordpress.com/about/) (WeMake Milan / Opencare.cc) | [Deborah Streahle](https://deborahstreahle.com/) (Yale) | [Nick Titus](https://fourthievesvinegar.org/) (Four Thieves Vinegar Collective) | [Kim Trogal](https://www.uca.ac.uk/staff-profiles/kim-trogal/) (UCA) | [Ana Vilenica](http://www.lsbu.ac.uk/about-us/people/people-finder/dr-ana-vilenica) (LSBU) | [Kandis Williams](https://www.cassandrapress.org/about) (Cassandra Press) | Kitty Worthing [(Docs Not Cops)](http://www.docsnotcops.co.uk/) + James Skinner [(Medact)](https://www.medact.org/)| [John Wilbanks](https://del-fi.org/jtw) (Sage Bionetworks/ FasterCures)


**Films** by [Kelly Gallagher](https://purpleriot.com/)(Syracuse University)

  
<br/>[**SEE ABSTRACTS**](/pages/abstracts/)
<br/>




**Schedule**


<span style="opacity: 0;">................. </span>| **DAY 1 - 19th June**
-------------|----------------------------------------------------------------------------
 10:00-10:30 | Registration and refreshments  
 10:30-11:00 | Welcome and Introduction: _[Gary Hall](http://www.garyhall.info/) + [Valeria Graziano](https://hcommons.org/members/valerix/)_  
 11:00-12:45 | **Session 1: CRIMINALIZATION OF CARE** (_Chair: Valeria Graziano_) 
             | **[Kirsten Forkert](https://www.bcu.ac.uk/media/research/research-staff/kirsten-forkert)** (BCU) + ** [Janna Graham](https://www.gold.ac.uk/visual-cultures/staff/graham-janna/) ** (Goldsmiths) + **[Victoria Mponda](http://conflictmemorydisplacement.com/)** (Conflict, Memory, Displacement project): _Social media and refugee solidarity networks in the face of state failures_     
             | ** [Jelka Kretzschmar + Franziska Wallner (Sea-Watch)](https://sea-watch.org/en/)  **: _Pirates of the Mediterranean – care on the high seas_   
             | **[Kitty Worthing (Docs Not Cops) + James Skinner (Medact)](http://www.docsnotcops.co.uk/)**: _#Patients Not Passports toolkit_   
 12:45-14:00 | LUNCH
 14:00-15:45 | **Session 2: HACKING HEALTHCARE** (_Chair: [Adrienne Evans](https://pureportal.coventry.ac.uk/en/persons/adrienne-evans)_)
             | **[John Wilbanks](https://del-fi.org/jtw)** (Sage Bionetworks/ FasterCures): _Open Science, DIY Bio, and Cheap Data (video conference)_
             | **[Zoe Romano](https://zoescope.wordpress.com/about/)** (WeMake Milan):  _Rebelling with Care_
             | **[Nick Titus (The Four Thieves Vinegar Collective)](https://fourthievesvinegar.org/)**: _I'm my own primary care provider: Taking back control with diy medicine in the 21st Century._
 15:45-16:15 | COFFEE BREAK
 16:15-18:00 | **Session 3: SOCIAL REPRODUCTION: WITHIN AND AGAINST** (_Chair: Marcell Mars_)
             | **[Maddalena Fragnito](https://www.maddalenafragnito.com/)**: _Soprasotto, a pirate kindergarten_
             | **[Toufic Haddad](https://cbrl.ac.uk/news/item/name/cbrl-s-kenyon-institute-welcomes-a-new-deputy-director)** (Kenyon Institute): _Excursions in Pirate Care in Palestine_
             | **[Deborah Streahle](https://deborahstreahle.com/)** (Yale): _Reclaiming the Dead: The Politics of Home Funerals in United States_
 18:00-18:30 | DRINKS RECEPTION  
 18:30-19:30 | **FILM, CREATIVITY, RESISTANCE**: Screening of works by experimental animator   **[Kelly Gallagher](https://purpleriot.com/)** (Syracuse University) + artist talk in conversation with **[Miriam De Rosa](https://pureportal.coventry.ac.uk/en/persons/miriam-de-rosa) ** 

<span style="opacity: 0;">................. </span>| **DAY 2 - 20th June**
-------------|----------------------------------------------------------------------------
 10:00-12:30 | **Session 4: PIRACY AS CARE pt 1** (_Chair: [Kaja Marczewska](https://pureportal.coventry.ac.uk/en/persons/kaja-marczewska)_)  
 	     | **[Gilbert B. Rodman](https://cla.umn.edu/about/directory/profile/rodman) ** (University of Minnesota): _Whose Culture? Our Culture!: Pirates as Cultural Care/Takers_ 
 	     | ** [Andrea Liu](http://replaceandrea.blogspot.com/)** (Goldsmiths): _Artist Strategies to Enact Interventions of Pirate Care_   	     
 	     | ** [Kandis Williams](https://www.cassandrapress.org/about)** (Cassandra Press): _Fusing Ethics and Aesthetics, a question of what we owe to future generations:  piracy, collage, redistribution and messy dissemination - the work of CASSANDRA PRESS_ 
 	     | (10 min. break) 
 	     | **PIRACY AS CARE pt 2**  
 	     | **[Agustina Andreoletti](http://www.agustinaandreoletti.com/info/)** (Academy of Media Arts Cologne): _Shadow Libraries: Using Art to Resist_
 	     | **[Marcell Mars](https://monoskop.org/Marcell_Mars)** + ** [Tomislav Medak](http://tom.medak.click/en/bio/)** (CPC): _Against innovation – Compromised institutional agency and acts of custodianship_ 
 12:30-14:00 | LUNCH
 14:00-15:45 | **Session 5: ETHICS OF COLLECTIVE CARING** (_Chair: [Janneke Adema](https://pureportal.coventry.ac.uk/en/persons/janneke-adema)_)
 	     | ** [Taraneh Fazeli](https://www.bemiscenter.org/residency/by_year/2018/taraneh-fazeli.html)** (curatorial fellow, Jan van Eyck Academie and Canaries collective): _Sick Time, Sleepy Time, Crip Time: Against Capitalism’s Temporal Bullying_
  	     | **[Power Makes Us Sick (PMS)](https://pms.hotglue.me/)**: _Illegalism, self-defense and accountability as strategies of community care_
 	     | **[Mijke van der Drift](https://goldsmiths.academia.edu/MijkevanderDrift) ** (Goldsmiths / Royal Academy of Art, The Hague/ School for New Dance, Amsterdam): _Caring for Transformative Justice._
 15:45-16:15 | COFFEE BREAK
 16:15-17:45 | **Session 6: SUBVERTING INFRASTRUCTURES** (_Chair: [Peter Conlin](https://coventry.academia.edu/PeterConlin)_)
 	     | **[Kim Trogal](https://www.uca.ac.uk/staff-profiles/kim-trogal/)** (UCA): _Confronting unjust urban infrastructures: repairing water connections as acts of care_ 
 	     | **[Derly Guzman (Planka)](https://planka.nu/eng/)**: _Freeriding Insurance and the global free public transport movement_  
 	     | ** [Ana Vilenica](http://www.lsbu.ac.uk/about-us/people/people-finder/dr-ana-vilenica)**: _Between autonomy and regulation: Grassroots pirate care in the external borderscape of the EU_ (video conference)
 17:45-18:00 | Closing remarks


  


<br/>
**Background:**


Punitive neoliberalism (Davies, 2016) has been repurposing, rather than dismantling, welfare state provisions such as healthcare, income support, housing and education (Cooper, 2017: 314). This mutation is reintroducing 'poor laws' of a colonial flavour, deepening the lines of discrimination between citizens and non-citizens (Mitropolous, 2012: 27), and reframing the family unit as the sole bearer of responsibility for dependants.

However, against this background of institutionalised 'negligence' (Harney & Moten, 2013: 31), a growing wave of mobilizations around care can be witnessed across a number of diverse examples: the recent Docs Not Cops campaign in the UK, refusing to carry out documents checks on migrant patients; migrant-rescue boats (such as those operated by Sea-Watch) that defy the criminalization of NGOs active in the Mediterranean; and the growing resistance to homelessness via the reappropriation of houses left empty by speculators (like PAH in Spain); the defiance of legislation making homelessness illegal (such as Hungary's reform of October 2018) or municipal decrees criminalizing helping out in public space (e.g. Food Not Bombs' volunteers arrested in 2017).

On the other hand, we can see initiatives experimenting with care as collective political practices have to operate in the narrow grey zones left open between different technologies, institutions and laws in an age some fear is heading towards 'total bureaucratization' (Graeber, 2015: 30). For instance, in Greece, where the bureaucratic measures imposed by the Troika decimated public services, a growing number of grassroots clinics set up by the Solidarity Movement have responded by providing medical attention to those without a private insurance. In Italy, groups of parents without recourse to public childcare are organizing their own pirate kindergartens (Soprasotto), reviving a feminist tradition first experimented with in the 1970s. In Spain, the feminist collective GynePunk developed a biolab toolkit for emergency gynaecological care, to allow all those excluded from the reproductive medical services  -  such as trans or queer women, drug users and sex workers  - to perform basic checks on their own bodily fluids. Elsewhere, the collective Women on Waves delivers abortion pills from boats harboured in international waters - and more recently, via drones - to women in countries where this option is illegal.

Thus pirate care, seen in the light of these processes - choosing illegality or existing in the grey areas of the law in order to organize solidarity - takes on a double meaning: Care as Piracy and Piracy as Care (Graziano, 2018).

There is a need to revisit piracy and its philosophical implications - such as sharing, openness, decentralization, free access to knowledge and tools (Hall, 2016) - in the light of transformations in access to social goods brought about by digital networks. It is important to bring into focus the modes of intervention and political struggle that collectivise access to welfare provisions as acts of custodianship (Custodians.online, 2015) and commoning (Caffentzis & Federici, 2014). As international networks of tinkerers and hackers are re-imagining their terrain of intervention, it becomes vital to experiment with a changed conceptual framework that speaks of the importance of the digital realm as a battlefield for the re-appropriation of the means not only of production, but increasingly, of social reproduction (Gutiérrez Aguilar _et al._, 2016). More broadly, media representations of these dynamics - for example in experimental visual arts and cinema - are of key importance. Bringing the idea of pirate ethics into resonance with contemporary modes of care thus invites different ways of imagining a paradigm change, sometimes occupying tricky positions vis-à-vis the law and the status quo.

The present moment requires a non-oppositional and nuanced approach to the mutual implications of care and technology (Mol et al., 2010: 14), stretching the perimeters of both. And so, while the seminal definition of care distilled by Joan Tronto and Berenice Fisher sees it as 'everything that we do to maintain, continue, and repair "our world" so that we can live in it as well as possible' (Tronto & Fisher, 1990: 40), contemporary feminist materialist scholars such as Maria Puig de La Bellacasa feel the need to modify these parameters to include 'relations [that] maintain and repair a world so that humans and non-humans can live in it as well as possible in a complex life-sustaining web' (Puig de La Bellacasa, 2017: 97). It is in this spirit that we propose to examine how can we learn to compose (Stengers, 2015) answers to crises across a range of social domains, and alongside technologies and care practices.

<br/>
**_Centre for Postdigital Cultures_**

[The Centre for Postdigital Cultures (CPC)](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/), a disruptive iteration of the Centre for Disruptive Media, brings together media theorists, practitioners, activists and artists. It draws on cross-disciplinary ideas associated with open and disruptive media, the posthuman, the posthumanities, the Anthropocene and the Capitalocene to help both 21st century society and the university respond to the challenges they face in relation to the digital at a global, national and local level. In particular, the CPC endeavours to promote the transformation to a more socially just and sustainable ‘post-capitalist’ knowledge economy.


<br/>
**_Contact:_** Email: info@pirate.care

[@PirateCare](https://twitter.com/PirateCare)
[Facebook page](https://www.facebook.com/Pirate-Care-1440264226110767/)