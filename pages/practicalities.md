.. title: Practicalities
.. slug: practicalities
.. date: 2019-05-19 23:57:00
.. author: . 


**From London by train**  
There are regular direct trains from London Euston to Coventry operated by LondonMidland and Virgin Trains. These services run regularly around every 10-20 minutes and the duration is between 1-2hours depending on which service you use. Tickets may be booked from [http://www.nationalrail.co.uk/](http://www.nationalrail.co.uk/).

**From Birmingham New Street by train**  
Getting to Coventry Rail station is very easy from Birmingham New Street Station. The duration of the journey is 20-30 minutes and there is at least one service every 15minutes.

**From Birmingham International Airport**  
If you are an international postgraduate or early career researcher and would be interested in attending the conference there are regular trains to Coventry direct from Birmingham International Airport. Once at BHX (Birmingham International Airport) you can choose to catch the train, coach or bus service to Coventry.

_By train_: from the Arrivals you should proceed to the shuttle service to the train station, which is well sign-posted. Once at the Rail Station in the airport, the journey to Coventry is about 10mins long.

_By coach:_ if choosing the coach service to Coventry Pool Meadow Bus Station you should consider buying the tickets [online](http://www.nationalexpress.com/home.aspx ). The journey takes about 20mins.

_By bus:_ the bus service 900 or 900A runs every 20mins from the bus stop in front of Departures.

Birmingham International Airport offers services to over 110 countries worldwide – for more information please see [here](http://www.want2gothere.com/bhx/destinations/).  
The campus of Coventry University itself is located in the middle of the city centre, less than 15 minutes’ walk from the railway station and 5-10 minutes’ walk from Coventry Pool Meadow Bus Station, making travel between accommodation and restaurants extremely easy as everything you need is in walking distance. There is a variety of global cuisines available to suit your tastes. If you would rather travel by taxi between the station and campus a single journey will cost you about £4.



## Accommodation

Coventry offers a wide range of accommodation options which can cater to a wide price range. Many of the hotels, bed and breakfasts, and guest houses of Coventry are within a short walking distance of the conference location such as:

**Ibis – Coventry Centre**  
_5-10 minutes walk from the campus_  
Mile lane  
CV1 2LN  
[Book](http://www.ibis.com/gb/hotel-2793-ibis-coventry-centre/index.shtml)

**Premier Inn – Coventry City Centre** (Belgrade Plaza)  
_10-15 minutes walk from campus_  
Bond Street  
CV1 4AH  
[Book](http://www.premierinn.com/en/hotel/COVBAR/coventry-city-centre-belgrade-plaza)

**Ramada**  
_10-15 minutes walk from campus_  
The Butts  
CV13GG  
[Book](http://www.ramadacoventry.co.uk/)

**Spire View Bed and Breakfast  
**_10 minutes walk from campus_  
36 Park Road  
CV1 2LD  
[Book](http://www.spireviewguesthouse.co.uk/index.html)

For a more extensive list of accommodation in Coventry please visit [www.visitcoventryandwarwickshire.co.uk/stay](http://www.visitcoventryandwarwickshire.co.uk/stay/)