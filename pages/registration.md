.. title: Registration
.. slug: registration
.. date: 2019-05-19 23:52:00
.. author: . 


This is a free event and open to the public, however we would greatly appreciate registrations to provide you all our guests with an appropriate hospitality and catering!

[**REGISTER HERE**](https://www.coventry.ac.uk/research/about-us/research-events/2019/pirate-care-conference/)
  